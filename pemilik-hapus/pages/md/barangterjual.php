<?php 
	include'proses/koneksi.php';
?>

<h2 align="center">DATA BARANG TERJUAL</h2>
<br>
<table class="table table-hover table-responsive table-striped" border="0">
	<thead>
	<tr>
		<th>No</th>
		<th>ID Barang</th>
		<th>ID Pembayaran</th>
		<th>Tanggal</th>
		<th>Pelanggan</th>
		<th>Nama Barang</th>
		<th>Berat</th>
		<th>Jumlah Barang</th>
		<th>Subtotal</th>
	</tr>
</thead>
	
<?php 
	$noter=1;
	$tampilterj = mysqli_query($koneksi,"SELECT * FROM pembayaran");
	while ($tampter = mysqli_fetch_array($tampilterj)) {

		?>
	<tr>
		<td><?php echo $noter++ ?></td>
		<td><?php echo $tampter['id_barang']; ?></td>
		<td><?php echo $tampter['id_pembayaran']; ?></td>
		<td><?php echo $tampter['id_pelanggan']; ?></td>
		<td><?php echo $tampter['jumlah_brg']; ?></td>
		<td><?php echo $tampter['total_berat']; ?></td>
		<td><?php echo $tampter['sub_harga']; ?></td>
	</tr>
	<?php
	}
?>
</table>
</div>