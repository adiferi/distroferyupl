<div align="left">
<h2 align="center">Input Barang</h2>
<form action="pages/md/proses/inputbarang.php" method="POST" enctype="multipart/form-data" >
<table border="0" class="table" >
<br>
	<tr>
		<td class="col-xs-2">Nama Barang</td>
		<td>
			<input type="text" name="nama_barang" class="form-control">
		</td>
	</tr>
	<tr>
		<td class="col-xs-2">Kategori</td>
		<td>
			<input type="text" name="kategori" class="form-control">
		</td>
	</tr>
	<tr>
		<td class="col-xs-2">Deskripsi</td>
		<td>
			<textarea name="deskripsi" class="form-control"> </textarea>
		</td>
	</tr>
	<tr>
		<td class="col-xs-2">Stok</td>
		<td>
			<input type="text" name="stok" class="form-control" onkeypress="return hanyaAngka(event)">
		</td>
	</tr>
	<tr>
		<td class="col-xs-2">Harga Barang</td>
		<td>
			<input type="text" name="harga_barang" class="form-control" onkeypress="return hanyaAngka(event)">
		</td>
	</tr>
	<tr>
		<td class="col-xs-2">Berat Barang</td>
		<td>
			<input type="text" name="berat_barang" class="form-control" onkeypress="return hanyaAngka(event)">
		</td>
	</tr>
	<tr>
		<td class="col-xs-2">Upload Gambar</td>
		<td>
			<input type="file" name="gambar">
		</td>
	</tr>
</table>
</div>
<div align="center">
	<input type="submit" value="Simpan" name=inputbarang class="btn btn-primary">
</div>
</form>

<script>
		function hanyaAngka(evt) {
		  var charCode = (evt.which) ? evt.which : event.keyCode
		   if (charCode > 31 && (charCode < 48 || charCode > 57))
 
		    return false;
		  return true;
		}
	</script>