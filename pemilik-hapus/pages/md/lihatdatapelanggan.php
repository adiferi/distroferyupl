<?php 
	include'proses/koneksi.php';
?>

<h2 align="center">DATA PELANGGAN</h2>
<br>
<table class="table table-hover table-responsive table-striped" border="0">
	<thead>
	<tr>
		<th>No</th>
		<th>Nama Pelanggan</th>
		<th>Username Pelanggan</th>
		<th>No Telepon</th>
		<th>Alamat</th>
		<th>Email</th>
		<th>Foto</th>
	</tr>
</thead>
	
<?php 
	$nopel=1;
	$tampilpel = mysqli_query($koneksi,"SELECT * FROM pelanggan");
	while ($tampel = mysqli_fetch_array($tampilpel)) {
		?>
	<tr>
		<td><?php echo $nopel++ ?></td>
		<td><?php echo $tampel['nama_pelanggan']; ?></td>
		<td><?php echo $tampel['username_pelanggan']; ?></td>
		<td><?php echo $tampel['no_telp']; ?></td>
		<td><?php echo $tampel['alamat']; ?></td>
		<td><?php echo $tampel['email']; ?></td>
		<td><img src="<?php echo "../fotopelanggan/".$tampel['foto']; ?>" width="200" height="200"></td>
		<td>
			<a class="btn btn-primary" role="button" href="pages/md/formgantipass.php?id_pelanggan=<?php echo $tampel['id_pelanggan']; ?>">Ganti Password</a>
			<a class="btn btn-danger" role="button" href="pages/md/proses/hapuspelanggan.php?id_pelanggan=<?php echo $tampel['id_pelanggan']; ?>">Hapus</a>
		</td>
	</tr>
	<?php
	}
?>
</table>
</div>