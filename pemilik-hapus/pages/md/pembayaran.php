<?php 
	include'proses/koneksi.php';
?>

<h2 align="center">DATA PEMBAYARAN</h2>
<br>
<table class="table table-hover table-responsive table-striped" border="0">
	<thead>
	<tr>
		<th>No</th>
		<th>ID Pembayaran</th>
		<th>ID Pelanggan</th>
		<th>Alamat Pengiriman</th>
		<th>Ekspedisi</th>
		<th>Total Bayar</th>
		<th>Tanggal Pembayaran</th>
		<th>Bukti</th>
	</tr>
</thead>
	
<?php 
	$nopem=1;
	$tampilpem = mysqli_query($koneksi,"SELECT * FROM pembayaran");
	while ($tampem = mysqli_fetch_array($tampilpem)) {
		
		?>
	<tr>
		<td><?php echo $nopem++ ?></td>
		<td><?php echo $tampem['id_pembayaran']; ?></td>
		<td><?php echo $tampem['id_pelanggan']; ?></td>
		<td><?php echo $tampem['alamat_pengiriman']; ?></td>
		<td><?php echo $tampem['ekspedisi']; ?></td>
		<td><?php echo $tampem['total_bayar']; ?></td>
		<td><?php echo $tampem['tanggal_pembayaran']; ?></td>
		<td><img src="<?php echo "../fotopembayaran/".$tampem['foto_pembayaran']; ?>" width="200" height="200"></td>
		
	</tr>
	<?php
	}
?>
</table>
</div>