<?php 
	include'proses/koneksi.php';
?>

<h2 align="center">STATUS PEMBAYARAN</h2>
<br>
<table class="table table-hover table-responsive table-striped" border="0">
	<thead>
	<tr>
		<th>No</th>
		<th>ID Status</th>
		<th>ID Pelanggan</th>
		<th>ID Pembayaran</th>
		<th>Status</th>
		<th>Resi</th>
	</tr>
</thead>
	
<?php 
	$nost=1;
	$tampilst = mysqli_query($koneksi,"SELECT * FROM statuspembayaran");
	while ($tamst = mysqli_fetch_array($tampilst)) {
		
		?>
	<tr>
		<td><?php echo $nost++ ?></td>
		<td><?php echo $tamst['id_status']; ?></td>
		<td><?php echo $tamst['id_pelanggan']; ?></td>
		<td><?php echo $tamst['id_pembayaran']; ?></td>
		<td><?php echo $tamst['status_pesan']; ?></td>
		<td><?php echo $tamst['resi']; ?></td>
		<td>
			<a class="btn btn-primary" role="button" href="pages/md/formgantistatus.php?id_status=<?php echo $tamst['id_status']; ?>">Ganti Status</a>
		</td>
		
	</tr>
	<?php
	}
?>
</table>
</div>