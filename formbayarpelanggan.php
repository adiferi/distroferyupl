<?php 
include 'koneksi.php';

session_start();
if(empty($_SESSION)){
	header("Location: index.php");
} else {}

$sumbyr = mysqli_query($koneksi, "SELECT SUM(sub_harga) as total FROM keranjang WHERE id_pelanggan='{$_SESSION['id_pelanggan']}'");
$totbyr = mysqli_fetch_array($sumbyr);
$totbyr1 = $totbyr['total'];
$totbyr2 = $totbyr['total'];


$sumbrt = mysqli_query($koneksi, "SELECT SUM(total_berat) FROM keranjang WHERE id_pelanggan='{$_SESSION['id_pelanggan']}'");
$totber = mysqli_fetch_array($sumbrt);
$totber2 = $totber['SUM(total_berat)'];

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Pembayaran <?php echo $_SESSION['id_pelanggan']; ?></title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="images/icons/favicon.png"/>
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/themify/themify-icons.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/elegant-font/html-css/style.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/slick/slick.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/lightbox2/css/lightbox.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<!--===============================================================================================-->
</head>
<body class="animsition">

	<!-- Header -->
	<header class="header1">
		<!-- Header desktop -->
		<div class="container-menu-header">

			<div class="wrap_header">
				<!-- Logo -->
				<a href="indexlogin.php" class="logo">
					<img src="images/icons/logo/logo.png" alt="IMG-LOGO">
				</a>

				<!-- Menu -->
				<div class="wrap_menu">
					<nav class="menu">
						<ul class="main_menu">
							<li>
								<a href="indexlogin.php">Home</a>
							</li>

							<li class="sale-noti">
								<a href="product.php">Produk</a>
							</li>


							<li>
								<a href="#">Pembayaran</a>
								<ul class="sub_menu">
									<li><a href="lihatstatuspembayaran.php">Lihat Status Pembayaran</a></li>
								</ul>
							</li>

							<li class="sale-noti">
								<a href="contact2.php">Kontak</a>
							</li>

							<li class="sale-noti">
								<a href="carapembelian2.php">Cara Pembelian</a>
							</li>

							<li>
								<a href="prosespelanggan/logoutpelanggan.php">Logout</a>
							</li>
						</ul>
					</nav>
				</div>

				<!-- Header Icon -->
				<div class="header-icons">
					<a href="profilpelanggan.php" class="header-wrapicon1 dis-block">
						<img src="images/icons/icon-header-01.png" class="header-icon1" alt="ICON">
					</a>

					<span class="linedivide1"></span>

					<a href="cart.php" class="header-wrapicon1 dis-block">
						<img src="images/icons/icon-header-02.png" class="header-icon1 js-show-header-dropdown" alt="ICON" >
					</a>
				</div>
			</div>
		</div>

		<!-- Header Mobile -->
		<div class="wrap_header_mobile">
			<!-- Logo moblie -->
			<a href="indexlogin.php" class="logo-mobile">
				<img src="images/icons/logo.png" alt="IMG-LOGO">
			</a>

			<!-- Button show menu -->
			<div class="btn-show-menu">
				<!-- Header Icon mobile -->
				<div class="header-icons-mobile">
					<a href="profilpelanggan.php" class="header-wrapicon1 dis-block">
						<img src="images/icons/icon-header-01.png" class="header-icon1" alt="ICON">
					</a>

					<span class="linedivide2"></span>

					<div class="header-wrapicon2">
						<a href="cart.php" class="header-wrapicon1 dis-block">
							<img src="images/icons/icon-header-02.png" class="header-icon1 js-show-header-dropdown" alt="ICON" >
						</a>
						
					</div>
				</div>

				<div class="btn-show-menu-mobile hamburger hamburger--squeeze">
					<span class="hamburger-box">
						<span class="hamburger-inner"></span>
					</span>
				</div>
			</div>
		</div>

		<!-- Menu Mobile -->
		<div class="wrap-side-menu" >
			<nav class="side-menu">
				<ul class="main-menu">

					<li class="item-topbar-mobile p-l-10">
						
					</li>

					<li class="item-menu-mobile">
						<a href="indexlogin.php">Home</a>
					</li>

					<li class="item-menu-mobile">
						<a href="product.php">Produk</a>
					</li>

					
					<li class="item-menu-mobile">
						<a href="lihatstatuspembayaran.php">Lihat Pembayaran</a>
					</li>

					<li class="item-menu-mobile">
						<a href="contact2.php">Kontak</a>
					</li>

					<li class="item-menu-mobile">
						<a href="carapembelian2.php">Cara Pembelian</a>

						<li class="item-menu-mobile">
							<a href="prosespelanggan/logoutpelanggan.php">Logout</a>
						</li>
					</ul>
				</nav>
			</div>
		</header>

		<form class="form-horizontal" action="prosespelanggan/bayarpelanggan.php" method="POST" enctype="multipart/form-data" >
			<fieldset>

				<div class="row">
					<div class="col-md-4">
						<!-- Form Name -->
						<h2 align="center">Pembayaran</h2>
						<br>
						<div align="center">			
							<!-- Text input-->
							<div class="form-group">
								<label class="col-md-8 control-label" align="left">Nama Bank</label>
								<div class="col-md-8">
									<input  type="text" class="form-control input-md" value="BCA" readonly>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-8 control-label" align="left">Nomor Rekening</label>
								<div class="col-md-8">
									<input  type="text" class="form-control input-md" value="0600183530" readonly>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-8 control-label" align="left">Atas Nama</label>
								<div class="col-md-8">
									<input  type="text" class="form-control input-md" value="Peri Waldi Wahyudi" readonly>
								</div>
							</div>
							<input type="hidden" name="asal" id="asal" value="501" /> 
						</div>

					</div>
					<div class="col-md-4">
						<div align="center">
							<div class="card col-md-12">
								<div class="card-header">
									<h3>Lokasi Pengiriman</h3>
								</div>
								<div class="card-body">
									<?php
	//Get Data Provinsi
									$curl = curl_init();
									curl_setopt_array($curl, array(
										CURLOPT_URL => "http://api.rajaongkir.com/starter/province",
										CURLOPT_RETURNTRANSFER => true,
										CURLOPT_ENCODING => "",
										CURLOPT_MAXREDIRS => 10,
										CURLOPT_TIMEOUT => 30,
										CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
										CURLOPT_CUSTOMREQUEST => "GET",
										CURLOPT_HTTPHEADER => array(
											"key: c3c57374c16d0476c44cc13a7cc7cc65"
										),
									));

									$response = curl_exec($curl);
									$err = curl_error($curl);
									echo "<div class='form-group'>";
									echo "<label  class='col-md-12 control-label' align='left'>Provinsi Tujuan</label><br>";
									echo "<div class='col-md-12'>";
									echo "<select name='provinsi' id='provinsi' class='form-control'>";
									echo "<option>Pilih Provinsi Tujuan</option>";
									$data = json_decode($response, true);
									for ($i=0; $i < count($data['rajaongkir']['results']); $i++) {
										echo "<option value='".$data['rajaongkir']['results'][$i]['province_id']."'>".$data['rajaongkir']['results'][$i]['province']."</option>";
									}
									echo "</select></div></div>";

									?>
									<div class="form-group">
										<label class="col-md-12 control-label" align="left">Kabupaten Tujuan</label>
										<div class="col-md-12">
											<select id="kabupaten" name="kabupaten" class="form-control"></select>
										</div>
									</div>

									<div class="form-group">
										<label class="col-md-12 control-label" align="left">Kurir</label><br>
										<div class="col-md-12">		
											<select id="kurir" name="kurir" onchange="myFunction()" class="form-control">
												<option> Pilih Ekspedisi</option>
												<option value="jne">JNE</option>
												<option value="tiki">TIKI</option>
												<option value="pos">POS INDONESIA</option>
											</select>
										</div>
									</div>
									<br>
									<div id="ongkir"></div>

								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="card col-md-8 mt-2">
							<div class="card-header">
								<h4>TARIF TOTAL</h4>
							</div>
							<div class="card-body">
								<div class="form-group">
									<label>Harga Produk</label>
									<input  type="text" class="form-control input-md" value="<?php echo $totbyr1 ?>" id="total_harga" readonly>
									<input type="hidden" name="detailkirim" id="detailkirim" class="form-control input-md" readonly>
								</div>

								<div class="form-group">
									<label>Total Berat (gram)</label>
									<input id="berat" type="text" name="berat" class="form-control input-md" value="<?php echo $totber2 ?>" readonly>
								</div>
								<div class="form-group">
									<label>Estimasi Waktu (Hari)</label>
									<input type="text" class="form-control" name="estimasi_waktu" id="estimasiwaktu" readonly="readonly">
								</div>

								<div>
									<label>Biaya Kirim (Rp)</label>
									<input type="text" class="form-control" name="estimasi_biaya" id="biayakirim" readonly="readonly">
								</div>

								<div>
									<label>Total Transaksi (Rp) </label>
									<input type="text" name="total" id="total_biaya_transaksi" class="form-control" readonly >
								</div>

								<div>
									<label>Detail Alamat </label>
									<textarea class="form-control" name="detail_alamat" required="true"></textarea>
								</div>

								<div>
									<div align="center">
										<br>
										<input type="submit" value="Bayar" class="btn btn-info w-100" name="kirimpembayaran" >
									</div>
								</div>
							</div>
						</div>
					</div>




					<input type="text" name="total_bayar" value="<?php echo $totbyr1 ?>" hidden>



					<!-- File Button --> 

				</div>

				<!-- Button -->

			</fieldset>
		</form>
	</div>


	<!-- Footer -->
	<footer class="bg6 p-t-45 p-b-43 p-l-45 p-r-45">
		<div class="flex-w p-b-90">
			<div class="w-size6 p-t-30 p-l-15 p-r-15 respon3">

			</div>
		</div>

		<div class="t-center p-l-15 p-r-15">
			<a href="#">
				<img class="h-size2" src="images/icons/paypal.png" alt="IMG-PAYPAL">
			</a>

			<a href="#">
				<img class="h-size2" src="images/icons/visa.png" alt="IMG-VISA">
			</a>

			<a href="#">
				<img class="h-size2" src="images/icons/mastercard.png" alt="IMG-MASTERCARD">
			</a>

			<a href="#">
				<img class="h-size2" src="images/icons/express.png" alt="IMG-EXPRESS">
			</a>

			<a href="#">
				<img class="h-size2" src="images/icons/discover.png" alt="IMG-DISCOVER">
			</a>

			<div class="t-center s-text8 p-t-20">
				Copyright © 2018 All rights reserved. | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
			</div>
		</div>
	</footer>



	<!-- Back to top -->
	<div class="btn-back-to-top bg0-hov" id="myBtn">
		<span class="symbol-btn-back-to-top">
			<i class="fa fa-angle-double-up" aria-hidden="true"></i>
		</span>
	</div>

	<!-- Container Selection1 -->
	<div id="dropDownSelect1"></div>



	<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/jquery/jquery-3.2.1.min.js"></script>
	<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/animsition/js/animsition.min.js"></script>
	<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/bootstrap/js/popper.js"></script>
	<script type="text/javascript" src="vendor/bootstrap/js/bootstrap.min.js"></script>
	<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/select2/select2.min.js"></script>
	<script type="text/javascript">
	$(".selection-1").select2({
		minimumResultsForSearch: 20,
		dropdownParent: $('#dropDownSelect1')
	});
</script>
<!--===============================================================================================-->
<script type="text/javascript" src="vendor/slick/slick.min.js"></script>
<script type="text/javascript" src="js/slick-custom.js"></script>
<!--===============================================================================================-->
<script type="text/javascript" src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
<script type="text/javascript" src="vendor/lightbox2/js/lightbox.min.js"></script>
<!--===============================================================================================-->
<script type="text/javascript" src="vendor/sweetalert/sweetalert.min.js"></script>
<script type="text/javascript">
$('.block2-btn-addcart').each(function(){
	var nameProduct = $(this).parent().parent().parent().find('.block2-name').php();
	$(this).on('click', function(){
		swal(nameProduct, "is added to cart !", "success");
	});
});

$('.block2-btn-addwishlist').each(function(){
	var nameProduct = $(this).parent().parent().parent().find('.block2-name').php();
	$(this).on('click', function(){
		swal(nameProduct, "is added to wishlist !", "success");
	});
});
</script>

<!--===============================================================================================-->
<script src="js/main.js"></script>

<script type="text/javascript">

$(document).ready(function(){
	$('#provinsi').change(function(){
		var prov = $('#provinsi').val();
		$.ajax({
			type : 'GET',
			url : 'cek_kabupaten.php',
			data :  'prov_id=' + prov,
			success: function (data) {
				$("#kabupaten").html(data);
			}
		});
	});
});

$("#kurir").change(function(){ 
	var asal = $('#asal').val(); 
	var kab = $('#kabupaten').val(); 
	var kurir = $('#kurir').val(); 
	var berat = $('#berat').val(); 

	$.ajax({ 
		type : 'POST', 
		url : 'rajaongkir/cek_ongkir.php', 
		data :  {'kab_id' : kab, 'kurir' : kurir, 'asal' : asal, 'berat' : berat}, 
		success: function (data) { 
			$("#ongkir").html(data); 
		} 
	}); 
}); 

</script>
</body>
</html>