<?php
$asal = $_POST['asal'];
$id_kabupaten = $_POST['kab_id'];
$kurir = $_POST['kurir'];
$berat = $_POST['berat'];

$curl = curl_init();
curl_setopt_array($curl, array(
	CURLOPT_URL => "http://api.rajaongkir.com/starter/cost",
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 30,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "POST",
	CURLOPT_POSTFIELDS => "origin=".$asal."&destination=".$id_kabupaten."&weight=".$berat."&courier=".$kurir."",
	CURLOPT_HTTPHEADER => array(
		"content-type: application/x-www-form-urlencoded",
		"key:fb3280c32f010001a7b436e31c66451b"
	),
));
$response = curl_exec($curl);
$err = curl_error($curl);
curl_close($curl);
if ($err) {
	echo "cURL Error #:" . $err;
} else {
	$data = json_decode($response, true);
}
?>

<?php echo $data['rajaongkir']['origin_details']['city_name'];?> ke <?php echo $data['rajaongkir']['destination_details']['city_name'];?> @<?php echo $berat;?>gram Kurir : <?php echo strtoupper($kurir); 
?>

<?php
for ($k=0; $k < count($data['rajaongkir']['results']); $k++) {
	?>
	<div title="<?php echo strtoupper($data['rajaongkir']['results'][$k]['name']);?>" style="padding:10px">
		<table class="table table-striped table-bordered">
			<tr>
				<th>No.</th>
				<th>Jenis Layanan</th>
				<th>ETD</th>
				<th>Tarif</th>
				<th>Aksi</th>
			</tr>
			<?php
			for ($l=0; $l < count($data['rajaongkir']['results'][$k]['costs']); $l++) {
				?>
				<tr>
					<td><?php echo $l+1;?></td>
					<td>
						<div style="font:bold 16px Arial"><?php echo $data['rajaongkir']['results'][$k]['costs'][$l]['service'];?></div>
						<div style="font:normal 11px Arial"><?php echo $data['rajaongkir']['results'][$k]['costs'][$l]['description'];?></div>
					</td>
					<td align="center">&nbsp;<?php echo $data['rajaongkir']['results'][$k]['costs'][$l]['cost'][0]['etd'];?> days</td>
					<td align="right"><?php echo number_format($data['rajaongkir']['results'][$k]['costs'][$l]['cost'][0]['value']);?></td>
					<td >
						<a href="#"
						data-etd="<?php echo $data['rajaongkir']['results'][$k]['costs'][$l]['cost'][0]['etd'];?>"
						data-tarif="<?php echo $data['rajaongkir']['results'][$k]['costs'][$l]['cost'][0]['value'];?>"
						class="btn btn-primary text-white pilihongkir">PILIH</a></td>
					</tr>
					<?php
				}
				?>
			</table>
			<script type="text/javascript">
			function formatRupiah(angka, prefix){
				var number_string = angka.toString(),
				split   		= number_string.split(','),
				sisa     		= split[0].length % 3,
				rupiah     		= split[0].substr(0, sisa),
				ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

					// tambahkan titik jika yang di input sudah menjadi angka ribuan
					if(ribuan){
						separator = sisa ? '.' : '';
						rupiah += separator + ribuan.join('.');
					}
					
					rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
					return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
				}

				$(document).ready(function(){ 
					$(".pilihongkir").click(function(){ 
						var hari = $(this).attr('data-etd');
						var cost = $(this).attr('data-tarif');
						var total_transaksi = $('#total_belanja').val();
						var detail_kirim = "<?php echo $data['rajaongkir']['origin_details']['city_name'];?> ke <?php echo $data['rajaongkir']['destination_details']['city_name'];?>";

						var harga_barang = $('#total_harga').val();

						var total_trx = parseInt(harga_barang) + parseInt(cost);

						$("#estimasiwaktu").val(hari);
						$("#biayakirim").val(cost);
						$("#detailkirim").val(detail_kirim);
						$("#total_biaya_transaksi").val(total_trx);
						});
					});
				</script>
			</div>
			<?php
		}
		?>