<?php 
	session_start();
	include'koneksi.php';
	if(empty($_SESSION)){
	  header("Location: index.php");
	} else{
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>STATUS PEMBAYARAN</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="images/icons/favicon.png"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/themify/themify-icons.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/elegant-font/html-css/style.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/slick/slick.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/lightbox2/css/lightbox.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
  <link rel="stylesheet" href="admin/plugins/ionslider/ion.rangeSlider.min.js">
  <!-- Theme style -->
  <link rel="stylesheet" href="admin/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="admin/plugins/iCheck/square/blue.css">
  <link rel="stylesheet" href="admin/bootstrap/css/bootstrap.min.css">

</head>
<body class="animsition">

	<!-- Header -->
	<header class="header1">
		<!-- Header desktop -->
		<div class="container-menu-header">

			<div class="wrap_header">
				<!-- Logo -->
				<a href="indexlogin.php" class="logo">
					<img src="images/icons/logo/logo.png" alt="IMG-LOGO">
				</a>

				<!-- Menu -->
				<div class="wrap_menu">
					<nav class="menu">
						<ul class="main_menu">
							<li>
								<a href="indexlogin.php">Home</a>
							</li>

							<li class="sale-noti">
								<a href="product.php">Produk</a>
							</li>

							
							<li>
								<a href="#">Pembayaran</a>
								<ul class="sub_menu">
									<li><a href="lihatstatuspembayaran.php">Lihat Status Pembayaran</a></li>
								</ul>
							</li>

							<li class="sale-noti">
								<a href="contact2.php">Kontak</a>
							</li>

							<li class="sale-noti">
								<a href="carapembelian2.php">Cara Pembelian</a>
							</li>

							<li>
								<a href="prosespelanggan/logoutpelanggan.php">Logout</a>
							</li>
						</ul>
					</nav>
				</div>

				<!-- Header Icon -->
				<div class="header-icons">
					<a href="profilpelanggan.php" class="header-wrapicon1 dis-block">
						<img src="images/icons/icon-header-01.png" class="header-icon1" alt="ICON">
					</a>

					<span class="linedivide1"></span>

					<a href="cart.php" class="header-wrapicon1 dis-block">
						<img src="images/icons/icon-header-02.png" class="header-icon1 js-show-header-dropdown" alt="ICON" >
						</a>
				</div>
			</div>
		</div>

		<!-- Header Mobile -->
		<div class="wrap_header_mobile">
			<!-- Logo moblie -->
			<a href="indexlogin.php" class="logo-mobile">
				<img src="images/icons/logo.png" alt="IMG-LOGO">
			</a>

			<!-- Button show menu -->
			<div class="btn-show-menu">
				<!-- Header Icon mobile -->
				<div class="header-icons-mobile">
					<a href="profilpelanggan.php" class="header-wrapicon1 dis-block">
						<img src="images/icons/icon-header-01.png" class="header-icon1" alt="ICON">
					</a>

					<span class="linedivide2"></span>

					<div class="header-wrapicon2">
						<a href="cart.php" class="header-wrapicon1 dis-block">
						<img src="images/icons/icon-header-02.png" class="header-icon1 js-show-header-dropdown" alt="ICON" >
						</a>
						
					</div>
				</div>

				<div class="btn-show-menu-mobile hamburger hamburger--squeeze">
					<span class="hamburger-box">
						<span class="hamburger-inner"></span>
					</span>
				</div>
			</div>
		</div>

		<!-- Menu Mobile -->
		<div class="wrap-side-menu" >
			<nav class="side-menu">
				<ul class="main-menu">

					<li class="item-topbar-mobile p-l-10">
						
					</li>

					<li class="item-menu-mobile">
						<a href="indexlogin.php">Home</a>
					</li>

					<li class="item-menu-mobile">
						<a href="product.php">Produk</a>
					</li>

					
					<li class="item-menu-mobile">
						<a href="lihatstatuspembayaran.php">Lihat Pembayaran</a>
					</li>

					<li class="item-menu-mobile">
						<a href="contact2.php">Kontak</a>
					</li>

					<li class="item-menu-mobile">
						<a href="carapembelian2.php">Cara Pembelian</a>

					<li class="item-menu-mobile">
						<a href="prosespelanggan/logoutpelanggan.php">Logout</a>
					</li>
				</ul>
			</nav>
		</div>
	</header>


	<div class="container">
	
	<h2>Transaksi Menunggu Pembayaran</h2>
	<label class="text-muted">Transaksi valid hingga 24 Jam sejak Order</label>
	<br>
	<table class="table table-hover table-responsive table-striped table-bordered" border="0">
		<thead>
		<tr>
			<th>No Transaksi</th>
			<th>Tgl Transaksi</th>
			<th>Status</th>
			<th>Total</th>
			<th>Status Pembayaran</th>
			<th>Aksi</th>
		</tr>
	</thead>
		
	<?php 
		$nopem=1;
		$tampilpem = mysqli_query($koneksi,"SELECT * FROM transaksi WHERE id_user='{$_SESSION['id_pelanggan']}' and status='chekout' ORDER BY id_transaksi DESC ");
		while ($pem = mysqli_fetch_array($tampilpem)) {
			?>
		<tr>
			<td><?php echo $pem['id_transaksi']; ?></td>
			<td><?php echo date('d-F-Y', strtotime($pem['tanggal'])) ?></td>
			<td><?php $exp =  date('Ymd', strtotime($pem['tanggal']. ' +1 day'));?>
				<?php $now =  date('Ymd', strtotime('now')) ;?>
				<?php 
				if ($exp<=$now) {
					echo "<span class='badge'>expired</span>";
				}else{
					echo "<span class='badge'>exp on ".$exp."</span>";
				}
				?>
			</td>
			<td>Rp. <?php echo $pem['total_transaksi']; ?></td>
			<td><a class="btn-xs btn-warning text-white">mengunggu pembayaran</a></td>
			<td>
				<?php 
				if ($exp<$now) {
					echo "<a href='#' class='btn btn-danger w-100' disabled>expired</a>";
				}else{
					echo "<a href='konfirmasi_pembayaran.php?trx=$pem[id_transaksi]' class='btn btn-success w-100'>Konfirmasi Pembayaran</a>";
				}
				?>
				
			</td>
		</tr>
		<?php
		}
	?>
	</table>

	<h2>Transaksi Proses Packing</h2>
	<br>
	<table class="table table-hover table-responsive table-striped table-bordered" border="0">
		<thead>
			<tr>
				<th>No Transaksi</th>
				<th>Tgl Transaksi</th>
				<th>Total</th>
				<th>Status </th>
			</tr>
		</thead>
		
		<?php 
		$nopem=1;
		$tampilpem = mysqli_query($koneksi,"SELECT * FROM transaksi WHERE id_user='{$_SESSION['id_pelanggan']}' and status='dibayar'");
		while ($pem = mysqli_fetch_array($tampilpem)) {
			?>
			<tr>
				<td><?php echo $pem['id_transaksi']; ?></td>
				<td><?php echo $pem['tanggal']; ?></td>
				<td>Rp. <?php echo $pem['total_transaksi']; ?></td>
				<td><a class="btn-xs btn-primary text-white">Proses Packing</a></td>
			</tr>
			<?php
		}
		?>
	</table>


	<h2>Transaksi Selesai</h2>
	<br>
	<table class="table table-hover table-responsive table-striped table-bordered" border="0">
		<thead>
		<tr>
			<th>No Transaksi</th>
			<th>Resi</th>
			<th>Total</th>
			<th>Status Pembayaran</th>
			<th>Aksi</th>
		</tr>
	</thead>
		
	<?php 
		$nopem=1;
		$tampilpem = mysqli_query($koneksi,"SELECT * FROM transaksi left join detail_kirim on transaksi.id_transaksi=detail_kirim.id_transaksi WHERE id_user='{$_SESSION['id_pelanggan']}' and status='terkirim' ");
		while ($pem = mysqli_fetch_array($tampilpem)) {
			?>
		<tr>
			<td><?php echo $pem['id_transaksi']; ?></td>
			<td><?php echo $pem['resi']; ?></td>
			<td>Rp. <?php echo $pem['total_transaksi']; ?></td>
			<td><a class="btn-xs btn-success text-white">Barang Sudah Di kirim</a></td>
			<td><a href="detailtranasksiselesai.php?trx=<?php echo $pem['id_transaksi']; ?>" class="btn btn-primary">Detail</a></td>
		</tr>
		<?php
		}
	?>
	</table>

	</div>

	</div>

	<!-- Footer -->
	<footer class="bg6 p-t-45 p-b-43 p-l-45 p-r-45">
		<div class="flex-w p-b-90">
			<div class="w-size6 p-t-30 p-l-15 p-r-15 respon3">
				
			</div>
		</div>

		<div class="t-center p-l-15 p-r-15">
			<a href="#">
				<img class="h-size2" src="images/icons/paypal.png" alt="IMG-PAYPAL">
			</a>

			<a href="#">
				<img class="h-size2" src="images/icons/visa.png" alt="IMG-VISA">
			</a>

			<a href="#">
				<img class="h-size2" src="images/icons/mastercard.png" alt="IMG-MASTERCARD">
			</a>

			<a href="#">
				<img class="h-size2" src="images/icons/express.png" alt="IMG-EXPRESS">
			</a>

			<a href="#">
				<img class="h-size2" src="images/icons/discover.png" alt="IMG-DISCOVER">
			</a>

			<div class="t-center s-text8 p-t-20">
				Copyright © 2018 All rights reserved. | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
			</div>
		</div>
	</footer>



	<!-- Back to top -->
	<div class="btn-back-to-top bg0-hov" id="myBtn">
		<span class="symbol-btn-back-to-top">
			<i class="fa fa-angle-double-up" aria-hidden="true"></i>
		</span>
	</div>

	<!-- Container Selection1 -->
	<div id="dropDownSelect1"></div>



<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/bootstrap/js/popper.js"></script>
	<script type="text/javascript" src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/select2/select2.min.js"></script>
	<script type="text/javascript">
		$(".selection-1").select2({
			minimumResultsForSearch: 20,
			dropdownParent: $('#dropDownSelect1')
		});
	</script>
<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/slick/slick.min.js"></script>
	<script type="text/javascript" src="js/slick-custom.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/lightbox2/js/lightbox.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/sweetalert/sweetalert.min.js"></script>
	<script type="text/javascript">
		$('.block2-btn-addcart').each(function(){
			var nameProduct = $(this).parent().parent().parent().find('.block2-name').php();
			$(this).on('click', function(){
				swal(nameProduct, "is added to cart !", "success");
			});
		});

		$('.block2-btn-addwishlist').each(function(){
			var nameProduct = $(this).parent().parent().parent().find('.block2-name').php();
			$(this).on('click', function(){
				swal(nameProduct, "is added to wishlist !", "success");
			});
		});
	</script>

<!--===============================================================================================-->
	<script src="js/main.js"></script>
	<script src="admin/plugins/iCheck/icheck.min.js"></script>
	<script>
	  $(function () {
	    $('input').iCheck({
	      checkboxClass: 'icheckbox_square-blue',
	      radioClass: 'iradio_square-blue',
	      increaseArea: '20%' /* optional */
	    });
	  });
	</script>

</body>
</html>
