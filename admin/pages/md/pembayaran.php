<?php 
include'proses/koneksi.php';
?>
<h2 align="center">Data Transaksi</h2>
<br>

<div class="box">
	<div class="box-body">
		<table class="table table-hover table-responsive table-striped table-bordered" border="0">
			<thead>
				<tr>
					<th>No</th>
					<th>ID Transaksi</th>
					<th>Tanggal Transaksi</th>
					<th>Pelanggan</th>
					<th>Total Transaksi</th>
					<th>Status</th>
					<th>Aksi</th>
				</tr>
			</thead>	
			<?php 
			$nopem=1;
			$tampilpem = mysqli_query($koneksi,"SELECT * FROM transaksi  left join pelanggan on pelanggan.id_user=transaksi.id_user");
			while ($tampem = mysqli_fetch_array($tampilpem)) {

				?>
				<tr>
					<td><?php echo $nopem++ ?></td>
					<!-- tanggal  id_user  total_transaksi  status    -->
					<td><?php echo $tampem['id_transaksi']; ?></td>
					<td><?php echo $tampem['tanggal']; ?></td>
					<td><?php echo $tampem['nama_pelanggan']; ?></td>
					<td><?php echo $tampem['total_transaksi']; ?></td>
					<td>
						<?php 
						if($tampem['status']=='dibayar'){
							echo "<a class='btn-xs btn-success'>Terbayar Menungu Konfirmasi admin</a>";
						}
						if($tampem['status']=='terkirim'){
							echo "<a class='btn-xs btn-primary'>".$tampem['status']."</a>";
						}
						if($tampem['status']=='terbayar'){
							echo "<a class='btn-xs btn-warning'>".$tampem['status']."</a>";
						}
						if($tampem['status']=='chekout'){
							echo "<a class='btn-xs btn-danger'>".$tampem['status']."</a>";
						}
						?>
					</td>
					<td><a href="index.php?page=detailpembayaran&trx=<?php echo $tampem['id_transaksi']; ?>" class="btn btn-primary">Detail</a></td>
				</tr>
				<?php
			}
			?>
		</table>	
	</div>
</div>