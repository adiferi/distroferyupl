<div class="box" id="printableArea">
	<div class="box-body">
		<h2 align="center">LAPORAN PENJUALAN</h2>
		<h4 align="center">Per Tanggal <?php echo $_POST['tgl_awal']; ?> - <?php echo $_POST['tgl_akhir']; ?></h4>
		<br>
		<div class="box">
			<div class="boc-body">
				<table class="table table-hover table-responsive table-striped table-bordered" border="0">
					<thead>
						<tr>
							<th>No</th>
							<th>ID DETAIL TRANSAKSI</th>
							<th>TANGGAL TRANSAKSI</th>
							<th>ID Detail</th>
							<th>Nama Pelanggan</th>
							<th>id barang</th>
							<th>Nama barang</th>
							<th>Jumlah Beli</th>
							<th>Harga Satuan</th>
							<td align="right">Total Transaksi</td>
						</tr>
					</thead>
					<?php 
					$nost = 1;
					$total=0;
					$totalb=0;
					$qry = "SELECT * FROM detail_transaksi 
						left join barang 
						on barang.id_barang=detail_transaksi.id_barang 
						left join detail_barang 
						on barang.id_barang=detail_barang.id_barang 
						join transaksi
						on transaksi.id_transaksi=detail_transaksi.id_transaksi 
						join pelanggan 
						on pelanggan.id_user=transaksi.id_user 
						join konfirmasi_pembayaran 
						on transaksi.id_transaksi=konfirmasi_pembayaran.id_transaksi 
						where transaksi.status='terkirim'
						and transaksi.tanggal <= '".$_POST['tgl_akhir']."' and transaksi.tanggal >='".$_POST['tgl_awal']."' ";
					$tampilst = mysqli_query($koneksi,$qry);
					while ($tamst = mysqli_fetch_array($tampilst)) {
						$total = $total + $tamst['total_transaksi'];
						$totalb = $totalb + $tamst['jumlah_beli'];
						?>
						<tr>
							<td><?php echo $nost++ ?></td>
							<td><?php echo $tamst['id_transaksi']; ?></td>
							<td><?php echo $tamst['tanggal']; ?></td>
							<td><?php echo $tamst['id']; ?></td>
							<td><?php echo $tamst['nama_pelanggan']; ?></td>
							<td><?php echo $tamst['id_barang']; ?></td>
							<td><?php echo $tamst['nama_barang']; ?> ( <?php echo $tamst['size']; ?> )</td>
							<td><?php echo $tamst['jumlah_beli']; ?> Pcs</td>
							<td>Rp.<?php echo $tamst['harga_barang']; ?></td>
							<td align="right">Rp.<?php echo $tamst['total_transaksi']; ?></td>
						</tr>
						<?php } ?>

						<tr>
							<td colspan="9" align="right"><b>TOTAL BARANG TERJUAL</b></td>
							<td align="right"><b><?php echo $totalb; ?> Pcs</b></td>
						</tr>
						<tr>
							<td colspan="9" align="right"><b>TOTAL NILAI PENJUALAN</b></td>
							<td align="right"><b>Rp.<?php echo $total; ?></b></td>
						</tr>
					</table>	
				</div>
				<div class="box-footer">
					<input type="button" class="btn btn-success" onclick="printDiv('printableArea')" value="Print" />
					<!-- <input type="button" onclick="printDiv('printableArea')" value="print a div!" /> -->
					<!-- <input type="button"  value="print a div!" /> -->
					<a href="index.php?page=barangterjual" class="btn btn-primary">Kembali</a>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
	function printDiv(divName) {
		var printContents = document.getElementById(divName).innerHTML;
		var originalContents = document.body.innerHTML;

		document.body.innerHTML = printContents;

		window.print();

		document.body.innerHTML = originalContents;
	}
</script>