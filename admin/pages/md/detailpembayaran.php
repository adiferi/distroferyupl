

<h2 align="center">DETAIL TRANSAKSI ( <?php echo $_GET['trx'] ?> )</h2>
<br>
<div class="row">
	<div class="col-md-7">
		<div class="box">
			<div class="box-body">
				<section class="invoice">
					<h3>Detail Transaksi</h3>
					<div class="row">
						<div class="col-md-12">
							<table class="table table-striped">
								<?php 
								include 'koneksi.php';
								$sql = "SELECT * FROM Transaksi where id_transaksi='".$_GET['trx']."' ";
									// echo $sql;
								$result = mysqli_query($koneksi, $sql);
								if (mysqli_num_rows($result) > 0) {
									while($row = mysqli_fetch_assoc($result)) {
										?>
										<tr>
											<td>Id transaksi</td>
											<td><?php echo $_GET['trx']; ?></td>
										</tr>
										<tr>
											<td>Tanggal Transaksi</td>
											<td><?php echo $row['tanggal'] ?></td>
										</tr>
										<tr>
											<td>Status Transaksi</td>
											<td><?php echo $row['status'] ?></td>
										</tr>
										<?php
									}
								} else {
									echo "0 results";
								}
								mysqli_close($koneksi);
								?>
							</table>
						</div>
						<div class="col-md-12">
							<h3>Detail Pengiriman</h3>
							<table class="table table-striped">
								<?php 
								include 'koneksi.php';
								$sql = "SELECT * FROM detail_kirim where id_transaksi='".$_GET['trx']."' ";
							// echo $sql;
								$result = mysqli_query($koneksi, $sql);
								if (mysqli_num_rows($result) > 0) {
									while($row = mysqli_fetch_assoc($result)) {
										?>
										<tr>
											<td>Pengiriman ke </td>
											<td><?php echo $row['tujuan'] ?></td>
										</tr>
										<tr>
											<td>Kurir</td>
											<td><?php echo $row['kurir'] ?></td>
										</tr>
										<tr>
											<td>Estimasi</td>
											<td><?php echo $row['estimasi'] ?> Hari</td>
										</tr>
										<tr>
											<td>Resi</td>
											<td><?php echo $row['resi'] ?> Hari</td>
										</tr>
										<tr>
											<td>detail</td>
											<td><?php echo $row['detail_alamat'] ?></td>
										</tr>
										<?php
									}
								} else {
									echo "0 results";
								}
								mysqli_close($koneksi);
								?>
							</table>
						</div>
					</div>
					<br>
					<h3>Daftar Item Terbeli </h3>
					<div class="row">
						<table class="table table-striped table-bordered">
							<thead>
								<th>#</th>
								<th>nama barang</th>
								<th>jumlah barang</th>
								<th>harga satuan</th>
								<th>subtot</th>	
							</thead>

							<?php 
							$no=1;
							include 'koneksi.php';
							$sql = "SELECT * FROM detail_transaksi left join barang on barang.id_barang=detail_transaksi.id_barang where id_transaksi='".$_GET['trx']."' ";
								// echo $sql;
							$result = mysqli_query($koneksi, $sql);
							if (mysqli_num_rows($result) > 0) {
								while($row = mysqli_fetch_assoc($result)) {
									?>
									<tr>
										<td><?php echo $no; ?></td>
										<td><?php echo $row['nama_barang']; ?></td>
										<td><?php echo $row['jumlah_beli']; ?> pcs</td>
										<td>Rp. <?php echo $row['harga_barang']; ?></td>
										<td align="right">Rp. <?php echo $row['sub_total']; ?></td>
									</tr>
									<?php
									$no++;
								}
							} else {
								echo "0 results";
							}
							mysqli_close($koneksi);
							?>
							<tr>
								<td colspan="4" align="right">Ongkir</td>
								<?php 
								$no=1;
								include 'koneksi.php';
								$sql = "SELECT biaya_kirim FROM detail_kirim where id_transaksi='".$_GET['trx']."' ";
								// echo $sql;
								$result = mysqli_query($koneksi, $sql);
								if (mysqli_num_rows($result) > 0) {
									while($row = mysqli_fetch_assoc($result)) {
										?>
										<td align="right" >Rp.<?php echo $row['biaya_kirim']; ?></td>
										<?php
									}}mysqli_close($koneksi);
									?>
								</tr>
								<tr>
									<td colspan="4" align="right">Total Yang Harus Di bayar</td>
									<?php 
									$no=1;
									include 'koneksi.php';
									$sql = "SELECT total_transaksi FROM transaksi where id_transaksi='".$_GET['trx']."' ";
								// echo $sql;
									$result = mysqli_query($koneksi, $sql);
									if (mysqli_num_rows($result) > 0) {
										while($row = mysqli_fetch_assoc($result)) {
											?>
											<td align="right" ><h3>Rp.<?php echo $row['total_transaksi']; ?></h3></td>
											<?php
										}}mysqli_close($koneksi);
										?>
									</tr>
								</table>
							</div>	

							<div class="row" align="right">
								<div align="right">
								</div>
							</div>	
						</section>
					</div>
				</div>	
			</div>
			<div class="col-md-5">
				<form action="#" method="post">
					<div class="box">
						<div class="box-head">
							<h3 align="center">Pembayaran Terkonfirmasi</h3>
						</div>
						<div class="box-body" align="center">
							<?php 
							include 'koneksi.php';
							$tampilst = mysqli_query($koneksi,"SELECT * FROM transaksi left join pelanggan on pelanggan.id_user=transaksi.id_user join konfirmasi_pembayaran on transaksi.id_transaksi=konfirmasi_pembayaran.id_transaksi where transaksi.id_transaksi='".$_GET['trx']."' ");
							while ($tamst = mysqli_fetch_array($tampilst)) {
								?>
								<?php echo $tamst['foto']; ?>
								<div class="row">
									<img src="../fotopembayaran/<?php echo $tamst['foto']; ?>"  class="img-responsive" width="304" height="236">	
								</div>
								<?php } ?>

								<br>
								<br>

							</div>
						</div>	
					</form>
				</div>
			</div>
		</div>
	</div>