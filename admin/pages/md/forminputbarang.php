<!-- id_barang  nama_barang  kategori  deskripsi  gambar  berat_barang   -->
<div class="box">
	<div class="box-body">
		<div align="left">
			<h2 align="center">Input Barang</h2>
			<form action="pages/md/proses/inputbarang.php" method="POST" enctype="multipart/form-data" class="form_input">
				<table border="0" class="table" >
					<br>
					<tr>
						<td class="col-xs-2">Nama Barang</td>
						<td>
							<input type="text" name="nama_barang" class="form-control">
						</td>
					</tr>
					<tr>
						<td class="col-xs-2">Kategori</td>
						<td>
							<!-- <input type="text" name="kategori" class="form-control"> -->
							<select name="kategori" class="form-control" >
								<?php 
								$dbss = mysqli_query($koneksi,"SELECT * FROM kategori_barang");
								while ($a = mysqli_fetch_array($dbss)) { ?>
								<option value="<?php echo $a['nama_tipe']; ?>">
									<?php echo $a['nama_tipe']; ?></option>
									<?php } ?>	
								</select>
							</td>
						</tr>
						<tr>

							<tr>
								<td class="col-xs-2">Deskripsi</td>
								<td>
									<textarea name="deskripsi" class="form-control"> </textarea>
								</td>
							</tr>

							<tr>
								<td class="col-xs-2">Harga</td>
								<td>
									<input type="text" name="harga" class="form-control" onkeypress="return hanyaAngka(event)">
								</td>
							</tr>
							<tr>
								<td class="col-xs-2">Berat Barang</td>
								<td>
									<input type="text" name="berat_barang" class="form-control" onkeypress="return hanyaAngka(event)">
								</td>
							</tr>

							<tr>
								<td class="col-xs-2">Upload Gambar</td>
								<td>
									<input type="file" name="gambar">
								</td>
							</tr>
						</table>

						<div class="box">
							<div class="box-header">
								<h4>Ukuran Harga Dan Stok</h4>
							</div>
							<div class="box-body">
								<table class="table table-striped table-bordered" id="dynamic_field">
									<thead>
										<th>Ukuran</th>
										<th>Stok (Pcs)</th>
										<th>Aksi</th>
									</thead>
									<body>
										<tr class="list">
											<td>
												<select class="form-control" name="size[]">
													<option value="">-PILIH UKURAN-</option>
													<option value="S">S</option>
													<option value="M">M</option>
													<option value="L">L</option>
													<option value="XL">XL</option>
													<option value="XXL">XXL</option>
													<option value="3XL">3XL</option>
												</select>
											</td>
											<td>
												<input type="number" class="form-control" name="stok[]">
											</td>
											<td>
												<a href="#" class="btn btn-primary" id="add"><i class="fa fa-plus"></i> Tambah</a>
												
											</td>
										</tr>
									</body>
								</table>
							</div>
						</div>
					</div>
					<div align="right">
						<input type="submit" value="Simpan" name=inputbarang class="btn btn-primary">
					</div>
				</form>
			</div>
		</div>

		<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<script>
		function hanyaAngka(evt) {
			var charCode = (evt.which) ? evt.which : event.keyCode
			if (charCode > 31 && (charCode < 48 || charCode > 57))

				return false;
			return true;
		}

		$(document).ready(function(){
			var i =0;
			$('#add').click(function(event){
						event.preventDefault();
				var html = '<tr class="list"><td><select class="form-control" name="size[]"><option value="">-PILIH UKURAN-</option><option value="S">S</option>'+
				'<option value="M">M</option>'+
				'<option value="L">L</option>'+
				'<option value="XL">XL</option>'+
				'<option value="XXL">XXL</option>'+
				'<option value="3XL">3XL</option>'+
				'</select></td><td><input type="number" class="form-control" name="stok[]">'+
				'</td><td><a href="#" class="btn btn-danger remove"><i class="fa fa-trash"></i> Hapus</a>'+
				'</td></tr>';
				$('#dynamic_field').append(html);
			});
		})

	$('#dynamic_field').on('click', '.remove', function(event) {
		event.preventDefault();
		// alert('hapus');
		$(this).parents('.list').remove();
	});
	</script>