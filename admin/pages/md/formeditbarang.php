<?php 
	include 'proses/koneksi.php';
	$id_barang = $_GET['id_barang'];
	$tampilbarang = mysqli_query($koneksi,"SELECT * FROM barang WHERE id_barang='$id_barang'");

?>
<!DOCTYPE html>
<html>
<head>
	<link href="../../../admin/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="../../../admin/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<title>Edit Barang</title>
</head>
<body>
<h2 align="center">EDIT BARANG</h2>
<form action="proses/editbarang.php" method="POST">
<table class="table">
<?php 
while ($editbarang = mysqli_fetch_array($tampilbarang)) {
?>
	<tr>
		<td class="col-xs-2">ID Barang</td>
		<td>
			<input type="text" name="id_barang" value="<?php echo $editbarang['id_barang']; ?>" class="form-control" readonly>
		</td>
	</tr>
	<tr>
		<td class="col-xs-2">Nama Barang</td>
		<td>
			<input type="text" name="nama_barang" value="<?php echo $editbarang['nama_barang']; ?>" class="form-control">
		</td>
	</tr>
	<tr>
		<td class="col-xs-2">Kategori</td>
		<td>
			<input type="text" name="kategori" value="<?php echo $editbarang['kategori']; ?>" class="form-control">
		</td>
	</tr>
	<tr>
		<td class="col-xs-2">Deskripsi</td>
		<td>
			<textarea name="deskripsi" class="form-control"><?php echo $editbarang['deskripsi']; ?></textarea>
		</td>
	</tr>
	<tr>
		<td class="col-xs-2">Stok</td>
		<td>
			<input type="text" name="stok" value="<?php echo $editbarang['stok']; ?>" class="form-control">
		</td>
	</tr>
	<tr>
		<td class="col-xs-2">Harga Barang</td>
		<td>
			<input type="text" name="harga_barang" value="<?php echo $editbarang['harga_barang']; ?>" class="form-control">
		</td>
	</tr>
	<tr>
		<td class="col-xs-2">Berat Barang</td>
		<td>
			<input type="text" name="berat_barang" value="<?php echo $editbarang['berat_barang']; ?>" class="form-control">
		</td>
	</tr>
	<tr>
		<td>Gambar</td>
		<td>
			<img src="<?php echo "gambarbarang/".$editbarang['gambar']; ?>" width="200" height="200">
		</td>
	</tr>
	<?php } ?>
</table>
<div align="center">
	<input type="submit" value="UPDATE" name="editbarang" class="btn btn-info">
</div>
</form>
</body>
</html>