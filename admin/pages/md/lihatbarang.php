<?php 
include'proses/koneksi.php';
?>

<h2 align="center">DATA BARANG</h2>
<br>
<div class="box">
	<div class="box-body">
		<table class="table table-hover table-responsive table-striped" border="0">
			<thead>
				<tr>
					<th>No</th>
					<th>Nama Barang</th>
					<th>Kategori</th>
					<th>Deskripsi</th>
					<th>Harga</th>
					<th>Berat Barang</th>
					<th>Stok</th>
					<th>Aksi</th>
				</tr>
			</thead>

			<?php 
			$nobarang=1;
			$tampilbarang = mysqli_query($koneksi,"SELECT * FROM barang");
			while ($barang = mysqli_fetch_array($tampilbarang)) {
				?>
				<tr>
					<td><?php echo $nobarang++ ?></td>
					<td><?php echo $barang['nama_barang']; ?></td>
					<td><?php echo $barang['kategori']; ?></td>
					<td><?php echo $barang['deskripsi']; ?></td>
					<td>Rp.<?php echo $barang['harga']; ?></td>
					<td><?php echo $barang['berat_barang']; ?> Gram</td>
					<td>
						<?php 
						$db = mysqli_query($koneksi,"SELECT * FROM detail_barang where id_barang='".$barang['id_barang']."' ");
						while ($dbl = mysqli_fetch_array($db)) {
							echo "<pre class='form-control'>".$dbl['size']." = ".$dbl['stok']." pcs</pre>";
						}
							?>
						</td>
						<td>
							<a class="btn btn-primary" role="button" href="index.php?page=formubahbarang&id_barang=<?php echo $barang['id_barang']; ?>">Edit</a>
							<a class="btn btn-danger" role="button" href="pages/md/proses/hapusbarang.php?id_barang=<?php echo $barang['id_barang']; ?>">Hapus</a>
						</td>
					</tr>
					<?php
				}
				?>
			</table>	
		</div>
	</div>
</div>