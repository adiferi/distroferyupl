<?php 
include'proses/koneksi.php';
?>

<h2 align="center">Konfirmasi Pembayaran</h2>
<br>
<div class="box">
	<div class="boc-body">
		<table class="table table-hover table-responsive table-striped" border="0">
			<thead>
				<tr>
					<th>No</th>
					<th>ID Status</th>
					<th>Nama Pelanggan</th>
					<th>Total Transaksi</th>
					<th>Waktu Terbayar</th>
					<th>Status</th>
				</tr>
			</thead>
			<?php 
			$nost=1;
			$tampilst = mysqli_query($koneksi,"SELECT * FROM transaksi left join pelanggan on pelanggan.id_user=transaksi.id_user join konfirmasi_pembayaran on transaksi.id_transaksi=konfirmasi_pembayaran.id_transaksi where transaksi.status='dibayar' ");
			while ($tamst = mysqli_fetch_array($tampilst)) {
				?>
				<tr>
					<td><?php echo $nost++ ?></td>
					<td><?php echo $tamst['id_transaksi']; ?></td>
					<td><?php echo $tamst['nama_pelanggan']; ?></td>
					<td>Rp.<?php echo $tamst['total_transaksi']; ?></td>
					<td><?php echo $tamst['waktu_pembayaran']; ?></td>
					<td>
						<a class="btn btn-primary"  role="button" href="index.php?page=ubahstatustransaksi&trx=<?php echo $tamst['id_transaksi']; ?>">Detail</a>
					</td>
				</tr>
				<?php
			}
			?>
		</table>	
	</div>
</div>