<li class="header">NAVIGASI</li>

<li class="treeview">
  <a href="#">
    <i class="fa fa-files-o"></i>
    <span>Data Barang</span>
  </a>
  <ul class="treeview-menu">
    <li><a href="?page=forminputkategoribarang"><i class="fa fa-circle-o"></i> Input kategori barang</a></li>
    <li><a href="?page=forminputbarang"><i class="fa fa-circle-o"></i> Input Barang</a></li>
    <li><a href="?page=lihatbarang"><i class="fa fa-circle-o"></i> Lihat Barang</a></li>
  </ul>
</li>

<li class="treeview">
  <a href="#">
    <i class="fa fa-files-o"></i>
    <span>Data Pelanggan</span>
  </a>
  <ul class="treeview-menu">
    <li><a href="?page=lihatdatapelanggan"><i class="fa fa-circle-o"></i>Lihat Data Pelanggan</a></li>
  </ul>
</li>


<li class="treeview">
  <a href="#">
    <i class="fa fa-files-o"></i>
    <span>Pembayaran</span>
  </a>
  <ul class="treeview-menu">
    <li><a href="?page=pembayaran"><i class="fa fa-circle-o"></i> Data Pembayaran</a></li>
    <li><a href="?page=statuspembayaran"><i class="fa fa-circle-o"></i> Konfirmasi Pembayaran</a></li>
  </ul>
</li>

<li class="treeview">
  <a href="#">
    <i class="fa fa-files-o"></i>
    <span>Laporan</span>
  </a>
  <ul class="treeview-menu">
    <li><a href="?page=barangterjual"><i class="fa fa-circle-o"></i> Data Barang Terjual</a></li>
  </ul>
</li>      