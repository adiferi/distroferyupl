<?php 
include 'koneksi.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Pembayaran</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="images/icons/favicon.png"/>
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/themify/themify-icons.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/elegant-font/html-css/style.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/slick/slick.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/lightbox2/css/lightbox.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<!--===============================================================================================-->
</head>
<body class="animsition" onload="print()">

	<div class="container  small">
		<section class="invoice">
			<h3>Nota Transaksi</h3>
			<div class="row">
				<div class="col-md-6">
					<table class="table ">
						<?php 
						include 'koneksi.php';
						$sql = "SELECT * FROM Transaksi where id_transaksi='".$_GET['trx']."' ";
							// echo $sql;
						$result = mysqli_query($koneksi, $sql);
						if (mysqli_num_rows($result) > 0) {
							while($row = mysqli_fetch_assoc($result)) {
								?>
								<tr>
									<td>Id transaksi</td>
									<td><?php echo $_GET['trx']; ?></td>
								</tr>
								<tr>
									<td>Tanggal Transaksi</td>
									<td><?php echo $row['tanggal'] ?></td>
								</tr>
								<tr>
									<td>Status Transaksi</td>
									<td><?php echo $row['status'] ?></td>
								</tr>
								<?php
							}
						} else {
							echo "0 results";
						}
						mysqli_close($koneksi);
						?>
					</table>
				</div>
				<div class="col-md-6">
					<table class="table">
						<?php 
						include 'koneksi.php';
						$sql = "SELECT * FROM detail_kirim where id_transaksi='".$_GET['trx']."' ";
							// echo $sql;
						$result = mysqli_query($koneksi, $sql);
						if (mysqli_num_rows($result) > 0) {
							while($row = mysqli_fetch_assoc($result)) {
								?>
								<tr>
									<td>Pengiriman ke </td>
									<td><?php echo $row['tujuan'] ?></td>
								</tr>
								<tr>
									<td>Kurir</td>
									<td><?php echo $row['kurir'] ?></td>
								</tr>
								<tr>
									<td>Estimasi</td>
									<td><?php echo $row['estimasi'] ?> Hari</td>
								</tr>
								<tr>
									<td>Detail Alamat</td>
									<td><?php echo $row['detail_alamat'] ?> Hari</td>
								</tr>
								<?php
							}
						} else {
							echo "0 results";
						}
						mysqli_close($koneksi);
						?>
					</table>
				</div>
			</div>
			<br>
			<h3>Daftar Item Terbeli </h3>
			<div class="row">
				<table class="table table-striped table-bordered">
					<thead>
						<th>#</th>
						<th>nama barang</th>
						<th>jumlah barang</th>
						<th>harga satuan</th>
						<th>subtot</th>	
					</thead>

					<?php 
					$no=1;
					include 'koneksi.php';
					$sql = "SELECT * FROM detail_transaksi left join barang on barang.id_barang=detail_transaksi.id_barang where id_transaksi='".$_GET['trx']."' ";
						// echo $sql;
					$result = mysqli_query($koneksi, $sql);
					if (mysqli_num_rows($result) > 0) {
						while($row = mysqli_fetch_assoc($result)) {
							?>
							<tr>
								<td><?php echo $no; ?></td>
								<td><?php echo $row['nama_barang']; ?></td>
								<td><?php echo $row['jumlah_beli']; ?> pcs</td>
								<td>Rp. <?php echo $row['harga_barang']; ?></td>
								<td align="right">Rp. <?php echo $row['sub_total']; ?></td>
							</tr>
							<?php
							$no++;
						}
					} else {
						echo "0 results";
					}
					mysqli_close($koneksi);
					?>
					<tr>
						<td colspan="4" align="right">Ongkir</td>
						<?php 
						$no=1;
						include 'koneksi.php';
						$sql = "SELECT biaya_kirim FROM detail_kirim where id_transaksi='".$_GET['trx']."' ";
						// echo $sql;
						$result = mysqli_query($koneksi, $sql);
						if (mysqli_num_rows($result) > 0) {
							while($row = mysqli_fetch_assoc($result)) {
								?>
								<td align="right" >Rp.<?php echo $row['biaya_kirim']; ?></td>
								<?php
							}}mysqli_close($koneksi);
							?>
						</tr>
						<tr>
							<td colspan="4" align="right">Total Yang Harus Di bayar</td>
							<?php 
							$no=1;
							include 'koneksi.php';
							$sql = "SELECT total_transaksi FROM transaksi where id_transaksi='".$_GET['trx']."' ";
						// echo $sql;
							$result = mysqli_query($koneksi, $sql);
							if (mysqli_num_rows($result) > 0) {
								while($row = mysqli_fetch_assoc($result)) {
									?>
									<td align="right" ><h3>Rp.<?php echo $row['total_transaksi']; ?></h3></td>
									<?php
								}}mysqli_close($koneksi);
								?>
							</tr>
						</table>
					</div>	

					<div class="row" align="right">
						<div align="right">
						</div>
					</div>	
				</section>
			</div>
			<!-- Back to top -->
			<div class="btn-back-to-top bg0-hov" id="myBtn">
				<span class="symbol-btn-back-to-top">
					<i class="fa fa-angle-double-up" aria-hidden="true"></i>
				</span>
			</div>

			<!-- Container Selection1 -->
			<div id="dropDownSelect1"></div>



			<!--===============================================================================================-->
			<script type="text/javascript" src="vendor/jquery/jquery-3.2.1.min.js"></script>
			<!--===============================================================================================-->
			<script type="text/javascript" src="vendor/animsition/js/animsition.min.js"></script>
			<!--===============================================================================================-->
			<script type="text/javascript" src="vendor/bootstrap/js/popper.js"></script>
			<script type="text/javascript" src="vendor/bootstrap/js/bootstrap.min.js"></script>
			<!--===============================================================================================-->
			<script type="text/javascript" src="vendor/select2/select2.min.js"></script>
			<script type="text/javascript">
			$(".selection-1").select2({
				minimumResultsForSearch: 20,
				dropdownParent: $('#dropDownSelect1')
			});
		</script>
		<!--===============================================================================================-->
		<script type="text/javascript" src="vendor/slick/slick.min.js"></script>
		<script type="text/javascript" src="js/slick-custom.js"></script>
		<!--===============================================================================================-->
		<script type="text/javascript" src="vendor/countdowntime/countdowntime.js"></script>
		<!--===============================================================================================-->
		<script type="text/javascript" src="vendor/lightbox2/js/lightbox.min.js"></script>
		<!--===============================================================================================-->
		<script type="text/javascript" src="vendor/sweetalert/sweetalert.min.js"></script>
		<script type="text/javascript">
		$('.block2-btn-addcart').each(function(){
			var nameProduct = $(this).parent().parent().parent().find('.block2-name').php();
			$(this).on('click', function(){
				swal(nameProduct, "is added to cart !", "success");
			});
		});

		$('.block2-btn-addwishlist').each(function(){
			var nameProduct = $(this).parent().parent().parent().find('.block2-name').php();
			$(this).on('click', function(){
				swal(nameProduct, "is added to wishlist !", "success");
			});
		});
	</script>

	<!--===============================================================================================-->
	<script src="js/main.js"></script>

	<script type="text/javascript">

	$(document).ready(function(){
		$('#provinsi').change(function(){
			var prov = $('#provinsi').val();
			$.ajax({
				type : 'GET',
				url : 'http://localhost/Distro/cek_kabupaten.php',
				data :  'prov_id=' + prov,
				success: function (data) {
					$("#kabupaten").html(data);
				}
			});
		});
	});

	$("#kurir").change(function(){ 
		var asal = $('#asal').val(); 
		var kab = $('#kabupaten').val(); 
		var kurir = $('#kurir').val(); 
		var berat = $('#berat').val(); 

		$.ajax({ 
			type : 'POST', 
			url : 'rajaongkir/cek_ongkir.php', 
			data :  {'kab_id' : kab, 'kurir' : kurir, 'asal' : asal, 'berat' : berat}, 
			success: function (data) { 
				$("#ongkir").html(data); 
			} 
		}); 
	}); 

</script>
</body>
</html>