<?php 
	session_start();
	include 'koneksi.php';
	if(empty($_SESSION)){
	  header("Location: index.php");
	} else{
	}
?>


<!DOCTYPE html>
<html lang="en">
<head>
	<title>Keranjang</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="images/icons/favicon.png"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/themify/themify-icons.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/elegant-font/html-css/style.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/slick/slick.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
</head>
<body class="animsition">

	<!-- Header -->
	<header class="header1">
		<!-- Header desktop -->
		<div class="container-menu-header">

			<div class="wrap_header">
				<!-- Logo -->
				<a href="indexlogin.php" class="logo">
					<img src="images/icons/logo/logo.png" alt="IMG-LOGO">
				</a>

				<!-- Menu -->
				<div class="wrap_menu">
					<nav class="menu">
						<ul class="main_menu">
							<li>
								<a href="indexlogin.php">Home</a>
							</li>

							<li class="sale-noti">
								<a href="product.php">Produk</a>
							</li>

							

							<li>
								<a href="#">Pembayaran</a>
								<ul class="sub_menu">
									<li><a href="lihatstatuspembayaran.php">Lihat Status Pembayaran</a></li>
								</ul>
							</li>

							<li class="sale-noti">
								<a href="contact2.php">Kontak</a>
							</li>

							<li class="sale-noti">
								<a href="carapembelian2.php">Cara Pembelian</a>
							</li>

							<li>
								<a href="prosespelanggan/logoutpelanggan.php">Logout</a>
							</li>
						</ul>
					</nav>
				</div>

				<!-- Header Icon -->
				<div class="header-icons">
					<a href="profilpelanggan.php" class="header-wrapicon1 dis-block">
						<img src="images/icons/icon-header-01.png" class="header-icon1" alt="ICON">
					</a>

					<span class="linedivide1"></span>

					<a href="cart.php" class="header-wrapicon1 dis-block">
						<img src="images/icons/icon-header-02.png" class="header-icon1 js-show-header-dropdown" alt="ICON" >
						</a>
				</div>
			</div>
		</div>

		<!-- Header Mobile -->
		<div class="wrap_header_mobile">
			<!-- Logo moblie -->
			<a href="indexlogin.php" class="logo-mobile">
				<img src="images/icons/logo.png" alt="IMG-LOGO">
			</a>

			<!-- Button show menu -->
			<div class="btn-show-menu">
				<!-- Header Icon mobile -->
				<div class="header-icons-mobile">
					<a href="profilpelanggan.php" class="header-wrapicon1 dis-block">
						<img src="images/icons/icon-header-01.png" class="header-icon1" alt="ICON">
					</a>

					<span class="linedivide2"></span>

					<div class="header-wrapicon2">
						<a href="cart.php" class="header-wrapicon1 dis-block">
						<img src="images/icons/icon-header-02.png" class="header-icon1 js-show-header-dropdown" alt="ICON" >
						</a>
						
					</div>
				</div>

				<div class="btn-show-menu-mobile hamburger hamburger--squeeze">
					<span class="hamburger-box">
						<span class="hamburger-inner"></span>
					</span>
				</div>
			</div>
		</div>

		<!-- Menu Mobile -->
		<div class="wrap-side-menu" >
			<nav class="side-menu">
				<ul class="main-menu">

					<li class="item-topbar-mobile p-l-10">
						
					</li>

					<li class="item-menu-mobile">
						<a href="indexlogin.php">Home</a>
					</li>

					<li class="item-menu-mobile">
						<a href="product.php">Produk</a>
					</li>


					<li class="item-menu-mobile">
						<a href="lihatstatuspembayaran.php">Lihat Pembayaran</a>
					</li>

					<li class="item-menu-mobile">
						<a href="contact2.php">Kontak</a>
					</li>

					<li class="item-menu-mobile">
						<a href="carapembelian2.php">Cara Pembelian</a>

					<li class="item-menu-mobile">
						<a href="prosespelanggan/logoutpelanggan.php">Logout</a>
					</li>
				</ul>
			</nav>
		</div>
	</header>

	<!-- Title Page -->
	

	<!-- Cart -->
	<section class="cart bgwhite p-t-70 p-b-100">
		<div class="container">
			<!-- Cart item -->
			<div class="container-table-cart pos-relative">
				<div class="wrap-table-shopping-cart bgwhite">
					<h2 align="center">Keranjang Barang</h2>
					<table class="table-shopping-cart">
						<tr class="table-head">
							<th class="column-1"></th>
							<th class="column-2">Nama Barang</th>
							<th class="column-2 p-l-70">Jumlah</th>
							<th class="column-2">Size</th>
							<th class="column-2">Harga</th>
							<th class="column-2">Total Berat</th>
							<th class="column-2">Total</th>
						</tr>
						
						<?php 
						$total_harga =0;
						$tampil = mysqli_query($koneksi,"SELECT 
							*
							FROM keranjang 
							left join detail_barang
							on keranjang.id_pesan=detail_barang.id_detail_barang
							left join barang
							on barang.id_barang=detail_barang.id_barang
							where keranjang.id_pelanggan ='{$_SESSION['id_pelanggan']}'");

							$no=1;
						while ($tam = mysqli_fetch_array($tampil)) {
							$total_harga = $total_harga+($tam['sub_harga']);
							$no++;
							?>
						<tr class="table-row">
							<form action="prosespelanggan/updatekeranjang.php?id_keranjang=<?php echo $tam['id_keranjang']; ?>" method="POST">
							<td class="column-1">
								<div class="cart-img-product b-rad-4 o-f-hidden">
									<img src="<?php echo "admin/pages/md/gambarbarang/".$tam['gambar']; ?>" alt="IMG-PRODUCT">
								</div>
							</td>
							<td class="column-2"><?php echo $tam['nama_barang']; ?></td>

							<td class="column-2">
								<div class="flex-w bo5 of-hidden w-size17">
									<button class="btn-num-product-down color1 flex-c-m size7 bg8 eff2">
										<i class="fs-12 fa fa-minus" aria-hidden="true"></i>
									</button>
									<input class="size8 m-text18 t-center num-product" type="number" name="jumlah_barang" value="<?php echo $tam['jumlah_barang'];?>">
									<button class="btn-num-product-up color1 flex-c-m size7 bg8 eff2">
										<i class="fs-12 fa fa-plus" aria-hidden="true"></i>
									</button>
								</div>
							</td>
							<td class="column-2"><?php echo $tam['size']; ?></td>
							
							<td class="column-2">Rp.<?php echo $tam['sub_harga']; ?></td>
							<td class="column-2"><?php echo $tam['total_berat']; ?> Gram</td>
							<td class="column-2"><a class="btn btn-danger" role="button" href="prosespelanggan/hapuskeranjang.php?id_keranjang=<?php echo $tam['id_keranjang']; ?>">Hapus</a></td>
							<td class="column-2"><button class="btn btn-primary" type="submit" name="updatekeranjangdrbarang">Update</button></td>
							</form>
						</tr>
					<?php  } ?>
						<tr>
							<td colspan="6" align="right"><h4>TOTAL YANG HARUS DI BAYAR</h4></td>
							<td align="right"><h4>Rp. <?php echo $total_harga; ?></h4></td>
						</tr>
				</div>
			</div>

			<div class="flex-w flex-sb-m p-t-25 p-b-25 bo8 p-l-35 p-r-60 p-lr-15-sm">
				<div class="flex-w flex-m w-full-sm">
					<div class="size11 bo4 m-r-10">
						
					</div>

					<div class="size12 trans-0-4 m-t-10 m-b-10 m-r-10">
						<!-- Button -->
						
					</div>
				</div>
				<div class="size10 trans-0-4 m-t-10 m-b-10">
					<!-- Button -->

					
					<?php 
					$a = mysqli_query($koneksi,"SELECT 
						barang.id_barang, 
						barang.nama_barang, 
						barang.gambar, 
						keranjang.id_keranjang, 
						keranjang.jumlah_barang, 
						keranjang.harga, 
						keranjang.total_berat, 
						keranjang.sub_harga, 
						detail_barang.size
						FROM barang left join keranjang
						on barang.id_barang=keranjang.id_barang 
						left join detail_barang
						on barang.id_barang=detail_barang.id_barang
						where keranjang.id_pelanggan ='{$_SESSION['id_pelanggan']}'");
					$tams = mysqli_fetch_array($a);
					// print_r($tams);
					if (!empty($tams)) {
						echo '<a class="flex-c-m sizefull bg1 bo-rad-23 hov1 s-text1 trans-0-4" href="formbayarpelanggan.php?">Bayar</a>';
					}else{
						echo '<a class="flex-c-m sizefull bg1 bo-rad-23 hov1 s-text1 trans-0-4" href="product.php">Belanja Sekarang</a>';
					}
					?>

				</div>
			</div>


		</div>
	</section>



	<!-- Footer -->
	<footer class="bg6 p-t-45 p-b-43 p-l-45 p-r-45">
		<div class="flex-w p-b-90">
			<div class="w-size6 p-t-30 p-l-15 p-r-15 respon3">
				
			</div>
		</div>

		<div class="t-center p-l-15 p-r-15">
			<a href="#">
				<img class="h-size2" src="images/icons/paypal.png" alt="IMG-PAYPAL">
			</a>

			<a href="#">
				<img class="h-size2" src="images/icons/visa.png" alt="IMG-VISA">
			</a>

			<a href="#">
				<img class="h-size2" src="images/icons/mastercard.png" alt="IMG-MASTERCARD">
			</a>

			<a href="#">
				<img class="h-size2" src="images/icons/express.png" alt="IMG-EXPRESS">
			</a>

			<a href="#">
				<img class="h-size2" src="images/icons/discover.png" alt="IMG-DISCOVER">
			</a>

			<div class="t-center s-text8 p-t-20">
				Copyright © 2018 All rights reserved. | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
			</div>
		</div>
	</footer>



	<!-- Back to top -->
	<div class="btn-back-to-top bg0-hov" id="myBtn">
		<span class="symbol-btn-back-to-top">
			<i class="fa fa-angle-double-up" aria-hidden="true"></i>
		</span>
	</div>

	<!-- Container Selection -->
	<div id="dropDownSelect1"></div>
	<div id="dropDownSelect2"></div>



<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/bootstrap/js/popper.js"></script>
	<script type="text/javascript" src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/select2/select2.min.js"></script>
	<script type="text/javascript">
		$(".selection-1").select2({
			minimumResultsForSearch: 20,
			dropdownParent: $('#dropDownSelect1')
		});

		$(".selection-2").select2({
			minimumResultsForSearch: 20,
			dropdownParent: $('#dropDownSelect2')
		});
	</script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>
